"""Parcial 1. Crear una aplicación de línea de comandos que permita registro, búsqueda,
edición y eliminación de artículos dentro de un sistema de inventario."""

import sys
from pymongo import MongoClient

MONGO_URI ='mongodb:///localhost'

client = MongoClient(MONGO_URI)

db = client['Inventario']
collection = db['productos']

def registrar_productos():
    producto = input("Digite el nombre del producto: ")
    codigo = input("Digite el codigo del producto: ")
    almacen = {"producto": producto, "codigo": codigo}
    collection.insert_one(almacen)
    print("El producto fue agregado exitosamente")
    opciones()

def editar_productos():
    print("Los productos que pueden ser modificados son: ")
    busq = collection.find({},{"_id":0, "codigo":0})
    for resultado in busq:
        print(resultado)
    producto=input("Escriba el nombre del producto que desea editar: ")
    almacen=1
    busqueda = collection.find({"producto":producto}, {"_id":0, "codigo":0})
    for resultado in busqueda:
        almacen=0
        print(f"El producto a modificar es: {resultado}")
    if almacen==1:
        print(f"El producto '{producto}' no existe")
    else:
        productoeditado=input("Escriba el nuevo producto: ")
        collection.update_one({"producto": producto},{"$set":{"producto": productoeditado}})
        print("El producto fue editado exitosamente")
        opciones()

def eliminar_producto():
    print("Los productos que pueden ser eliminados son: ")
    busq = collection.find({}, {"_id": 0, "codigo": 0})
    for resultado in busq:
        print(resultado)
    producto = input("Escriba el nombre del producto que desea eliminar: ")
    almacen = 1
    busqueda = collection.find({"producto": producto}, {"_id": 0, "codigo": 0})
    for resultado in busqueda:
        almacen = 0
        print(f"El producto a eliminar es: {resultado}")
    if almacen == 1:
        print(f"El producto '{producto}' no existe")
    else:
        collection.delete_one({"producto": producto})
        print("El producto se ha eliminado exitosamente")
        opciones()

def buscar_productos():
    producto = input ("Escriba el nombre producto que desea buscar: ")
    busqueda = collection.find({"producto":producto},{"_id":0})
    almacen = 1
    for resultado in busqueda:
        almacen = 0
        print(resultado)
    if almacen == 1:
        print(f"El producto '{producto}' no existe")
    opciones()

def opciones():
    print("Bienvenidos al Sistema de Inventario")
    print("1- Registrar productos")
    print("2- Editar producto existente")
    print("3- Eliminar producto existente")
    print("4- Buscar codigo de productos")
    print("5- Salir")
    opcion=input("Seleccione una opcion:")
    if opcion=='1':
        registrar_productos()
    elif opcion == '2':
        editar_productos()
    elif opcion=='3':
        eliminar_producto()
    elif opcion=='4':
         buscar_productos()
    elif opcion=='5':
        print("Regrese pronto al sistema de inventario")
        sys.exit()
    else:
        input("Seleccione una opcion valida !!!")
        opciones()
opciones()
